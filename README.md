# Compact Google Browser Extension

## [Deprecated]: This project has been merged into [compact-website-styles](https://gitlab.com/siphomateke/compact-website-styles)

Makes several google websites more compact. At the moment:

- Gmail
- Google My Activity
- Google Contacts
- Google Calendar
- Google Drive
- Google News
